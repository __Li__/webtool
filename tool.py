import os
import re
import shutil

if os.name == "nt":
	sep = "\\"
else:
	sep = "/"

hpts = []
tpls = []

drcmp = list(os.walk('.'))

class HPT:
	def __init__(self, name, txt):
		self.name = name
		self.txt = txt
		self.parts = []
		self.tags = {}
	
	def process(self):
		print "Processing", self.name+".hpt", "..."
		regexp = re.compile("(.*)\s(.*)")
		pt = self.txt
		opTg = pt.find("+#")
		edTg = pt.find("#+")
		ct = 0
		while opTg != -1 and edTg != -1:
			tg = pt[opTg+2:edTg]
			if regexp.match(tg) or tg == '':
				opTg = max(opTg, edTg)+2+pt[max(opTg, edTg)+2:].find("+#")
				edTg = max(opTg, edTg)+2+pt[max(opTg, edTg)+2:].find("#+")
			else:
				self.parts.append(pt[:opTg])
				self.tags[tg] = ct
				ct += 1
				pt = pt [edTg+2:]
				opTg = pt.find("+#")
				edTg = pt.find("#+")
		self.parts.append(pt)

class TPL:
	def __init__(self, path, file, txt):
		self.path = path
		self.file = file
		self.txt = txt
		self.fills = {}
	
	def process(self):
		print "Processing", self.file, "..."
		lines = self.txt.split()
		if len(lines) == 0 or lines[0][0] != ':':
			print "Corrupt tpl file:", file+"!"
		else:
			self.hpt = lines.pop(0)[1:]
			regexp = re.compile("(.*)\s(.*)")
			while len(lines) != 0:
				key = ""
				st = ""
				line = lines.pop(0)
				while len(lines) != 0 and (regexp.match(line) or line == '@' or line[0] != '@'):
					line = lines.pop(0)
				key = line[1:]
				while len(lines) != 0:
					line = lines.pop(0)
					if line == "/"+key:
						if not(key in self.fills):
							self.fills[key] = st
						else:
							print "Tag", key, "is already filled with information."
						break
					else:
						st += line+"\n"
			for key in self.fills:
				if self.fills[key][:2] == "*#" and not(regexp.match(self.fills[key][2:].strip())):
					if self.fills[key][2:].strip() in self.fills:
						self.fills[key] = self.fills[self.fills[key][2:].strip()]
					else:
						print "Tag", self.fills[key][2:], "doesn't exist in this tpl!"

def compile(tpl):
	for hpt in hpts:
		if tpl.hpt == hpt.name:
			print "HPT found, start compiling", tpl.file, "..."
			parts = ['' for x in xrange(len(hpt.tags)*2+1)]
			for key in tpl.fills:
				if key in hpt.tags:
					parts[hpt.tags[key]*2+1] = tpl.fills[key]
				else:
					print "HPT doesn't have tag \""+key+"\""
			for i, txt in enumerate(hpt.parts):
				parts[i*2] = txt
			st = "".join(parts)
			f = open(newdir+tpl.path[1:]+sep+(".".join(tpl.file.split('.')[:-1])), 'w')
			f.write(st)
			f.close()
			break

def cpy(path, file):
	print "Copying", file, "..."
	old = os.getcwd()+path[1:]+sep+file
	new = newdir+path[1:]+sep+file
	shutil.copyfile(old, new)

def processTpl(path, file):
	print "Reading", file, "..."
	f = open(path+sep+file, "r")
	tpls.append(TPL(path, file, f.read()))
	f.close()

def processHpt(path, file):
	print "Reading", file, "..."
	f = open(path+sep+file, "r")
	hpts.append(HPT(".".join(file.split('.')[:-1]), f.read()))
	f.close()

def getProcessingProcedure(fn):
	ext = fn.split('.')[-1]
	if ext == "tpl":
		return processTpl
	elif ext == "hpt":
		return processHpt
	else:
		return cpy

def delFl(p, f):
	f = f.replace("*", "(.*)")
	regexp = re.compile(f)
	for i in xrange(len(drcmp)):
		if drcmp[i][0] == p:
			drcmp[i] = (drcmp[i][0], drcmp[i][1], [x for x in drcmp[i][2] if not(regexp.match(x))])
			break

if "ignore" in drcmp[0][2]:
	print "Processing ignores ..."
	f = open("ignore", 'r')
	for file in f.read().strip().split():
		info = re.split(r"[\\/]+", file)
		path = sep.join(["."]+info[:-1])
		fn = info.pop()
		delFl(path, fn)
	f.close

fc = 0
newdir = os.getcwd()+"Compiled"
while os.path.exists(newdir):
	fc += 1
	newdir = os.getcwd()+"Compiled"+str(fc)
os.makedirs(newdir)
for path, dr, files in drcmp:
	for d in dr:
		os.makedirs(newdir+path[1:]+sep+d)
	for file in files:
		getProcessingProcedure(file)(path, file)

for h in hpts:
	h.process()

for t in tpls:
	t.process()
	compile(t)
print "Done!"
